# Platyca Add-on

## Depends on:

* https://gitlab.com/platyca/platyca-on-todesweb

## Install

```
cd .../src
sudo make install
```

## Run

Same as [Platica on Todesweb](https://gitlab.com/platyca/platyca-on-todesweb).

Reboot Platica on Todesweb to load this add-on.
